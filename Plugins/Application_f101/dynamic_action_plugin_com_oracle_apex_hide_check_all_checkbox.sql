set define off
set verify off
set serveroutput on size 1000000
set feedback off
WHENEVER SQLERROR EXIT SQL.SQLCODE ROLLBACK
begin wwv_flow.g_import_in_progress := true; end; 
/
 
--       AAAA       PPPPP   EEEEEE  XX      XX
--      AA  AA      PP  PP  EE       XX    XX
--     AA    AA     PP  PP  EE        XX  XX
--    AAAAAAAAAA    PPPPP   EEEE       XXXX
--   AA        AA   PP      EE        XX  XX
--  AA          AA  PP      EE       XX    XX
--  AA          AA  PP      EEEEEE  XX      XX
prompt  Set Credentials...
 
begin
 
  -- Assumes you are running the script connected to SQL*Plus as the Oracle user APEX_040000 or as the owner (parsing schema) of the application.
  wwv_flow_api.set_security_group_id(p_security_group_id=>nvl(wwv_flow_application_install.get_workspace_id,4778318160368125));
 
end;
/

begin wwv_flow.g_import_in_progress := true; end;
/
begin 

select value into wwv_flow_api.g_nls_numeric_chars from nls_session_parameters where parameter='NLS_NUMERIC_CHARACTERS';

end;

/
begin execute immediate 'alter session set nls_numeric_characters=''.,''';

end;

/
begin wwv_flow.g_browser_language := 'en'; end;
/
prompt  Check Compatibility...
 
begin
 
-- This date identifies the minimum version required to import this file.
wwv_flow_api.set_version(p_version_yyyy_mm_dd=>'2010.05.13');
 
end;
/

prompt  Set Application ID...
 
begin
 
   -- SET APPLICATION ID
   wwv_flow.g_flow_id := nvl(wwv_flow_application_install.get_application_id,101);
   wwv_flow_api.g_id_offset := nvl(wwv_flow_application_install.get_offset,0);
null;
 
end;
/

prompt  ...plugins
--
--application/shared_components/plugins/dynamic_action/com_oracle_apex_hide_check_all_checkbox
 
begin
 
wwv_flow_api.create_plugin (
  p_id => 861125740797674335 + wwv_flow_api.g_id_offset
 ,p_flow_id => wwv_flow.g_flow_id
 ,p_plugin_type => 'DYNAMIC ACTION'
 ,p_name => 'COM.ORACLE.APEX.HIDE_CHECK_ALL_CHECKBOX'
 ,p_display_name => 'Hide "Check All" Tabular Form Checkbox'
 ,p_category => 'INIT'
 ,p_image_prefix => '#PLUGIN_PREFIX#'
 ,p_plsql_code => 
'function hide_check_all_checkbox ('||chr(10)||
'    p_dynamic_action in apex_plugin.t_dynamic_action,'||chr(10)||
'    p_plugin         in apex_plugin.t_plugin )'||chr(10)||
'    return apex_plugin.t_dynamic_action_render_result'||chr(10)||
'is'||chr(10)||
'    l_result apex_plugin.t_dynamic_action_render_result;'||chr(10)||
'begin'||chr(10)||
'    l_result.javascript_function := ''function(){apex.jQuery("#CHECK\\$01").children().css("visibility", "hidden");}'';'||chr(10)||
'    return l_result;'||chr(10)||
'end h'||
'ide_check_all_checkbox;'
 ,p_render_function => 'hide_check_all_checkbox'
 ,p_help_text => '<p>'||chr(10)||
'	This dynamic action plug-in should be called for the event type &quot;Page Load&quot; (use the advanced dynamic action wizard) and will hide the &quot;Check All&quot; checkbox of a tabular form to prevent that users mark too many records for deletion.</p>'||chr(10)||
''
 ,p_version_identifier => '1.0'
  );
null;
 
end;
/

commit;
begin 
execute immediate 'begin dbms_session.set_nls( param => ''NLS_NUMERIC_CHARACTERS'', value => '''''''' || replace(wwv_flow_api.g_nls_numeric_chars,'''''''','''''''''''') || ''''''''); end;';
end;
/
set verify on
set feedback on
prompt  ...done
